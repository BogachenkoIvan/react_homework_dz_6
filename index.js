let field_size = 8;
let mine = Math.floor((field_size * field_size) / 6);

let board = [];
let flags = 0;
let revealedCount = 0;
let gameOver = false;

const boardElement = document.getElementById('board');
const restartButton = document.getElementById('restartButton');
const flagCountElement = document.getElementById('flagCount');
const mineCountElement = document.getElementById('mineCount');
const difficultySelect = document.getElementById('difficultySelect');

function createBoard() {
  board = [];
  flags = 0;
  revealedCount = 0;
  gameOver = false;

  for (let row = 0; row < field_size; row++) {
    board[row] = [];
    for (let col = 0; col < field_size; col++) {
      board[row][col] = {
        mine: false,
        revealed: false,
        flagged: false,
        value: 0
      };
    }
  }

  let minesPlaced = 0;
  while (minesPlaced < mine) {
    const row = Math.floor(Math.random() * field_size);
    const col = Math.floor(Math.random() * field_size);
    if (!board[row][col].mine) {
      board[row][col].mine = true;
      minesPlaced++;
    }
  }

  calculateValues();
}

function calculateValues() {
  for (let row = 0; row < field_size; row++) {
    for (let col = 0; col < field_size; col++) {
      const cell = board[row][col];
      if (!cell.mine) {
        cell.value = countAdjacentMines(row, col);
      }
    }
  }
}

function countAdjacentMines(row, col) {
  let count = 0;

  for (let r = row - 1; r <= row + 1; r++) {
    for (let c = col - 1; c <= col + 1; c++) {
      if (isValidCell(r, c) && board[r][c].mine) {
        count++;
      }
    }
  }

  return count;
}

function renderBoard() {
    boardElement.innerHTML = '';
  
    for (let row = 0; row < field_size; row++) {
      const tr = document.createElement('tr');
      for (let col = 0; col < field_size; col++) {
        const td = document.createElement('td');
        const cell = board[row][col];
  
        if (cell.revealed) {
          if (cell.mine) {
            td.innerHTML = '<img src="./img/mine.png" alt="Міна" />';
          } else if (cell.value > 0) {
            td.innerText = cell.value;
          }
          td.style.backgroundColor = 'white'; 
        } else if (cell.flagged) {
          td.innerHTML = '<img src="./img/flag.png" alt="Флаг" />';
        }
  
        td.addEventListener('mousedown', (e) => {
          if (gameOver) return;
          if (e.button === 0) {
            revealCell(row, col);
            }
        });
  
        td.addEventListener('contextmenu', (e) => {
          e.preventDefault(); 
          toggleFlag(row, col);
        });
  
        tr.appendChild(td);
      }
      boardElement.appendChild(tr);
    }
  }
  

function revealCell(row, col) {
  const cell = board[row][col];

  if (cell.flagged || cell.revealed) return;

  cell.revealed = true;
  revealedCount++;

  if (cell.mine) {
    gameOver = true;
    revealMines();
    alert('Гра закінчена. Ви програли!');

  } else if (cell.value === 0) {
    revealEmptyCells(row, col);
  }

  if (revealedCount === (field_size * field_size) - mine) {
    gameOver = true;
    alert('Гра закінчена. Ви виграли!');
  }

  renderBoard();
}

function toggleFlag(row, col) {
  const cell = board[row][col];

  if (!cell.revealed) {
    cell.flagged = !cell.flagged;
    flags += cell.flagged ? 1 : -1;
    flagCountElement.innerText = flags;
    renderBoard();
  }
}

function revealEmptyCells(row, col) {
  for (let r = row - 1; r <= row + 1; r++) {
    for (let c = col - 1; c <= col + 1; c++) {
      if (isValidCell(r, c)) {
        const cell = board[r][c];
        if (!cell.revealed) {
          revealCell(r, c);
        }
      }
    }
  }
}

function revealMines() {
  for (let row = 0; row < field_size; row++) {
    for (let col = 0; col < field_size; col++) {
      if (board[row][col].mine) {
        board[row][col].revealed = true;
      }
    }
  }
}

function isValidCell(row, col) {
  return row >= 0 && row < field_size && col >= 0 && col < field_size;
}

function handleDifficultyChange() {
  const selectedDifficulty = difficultySelect.value;

  switch (selectedDifficulty) {
    case 'easy':
      field_size = 8;
      break;
    case 'medium':
      field_size = 12;
      break;
    case 'hard':
      field_size = 16;
      break;
    default:
      field_size = 8;
  }

  mine = Math.floor((field_size * field_size) / 6);

  initGame();
}

function initGame() {
  createBoard();
  renderBoard();
  flagCountElement.innerText = flags;
  mineCountElement.innerText = mine;
}

difficultySelect.addEventListener('change', handleDifficultyChange);
restartButton.addEventListener('click', initGame);

initGame();
